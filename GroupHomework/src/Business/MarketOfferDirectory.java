/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author murta
 */
public class MarketOfferDirectory {
    
    private ArrayList<MarketOffer> marketOfferList;

    public MarketOfferDirectory() {
       marketOfferList = new ArrayList<MarketOffer>();
    }

    public ArrayList<MarketOffer> getMarketOfferList() {
        return marketOfferList;
    }

    public void setMarketOfferList(ArrayList<MarketOffer> marketOfferList) {
        this.marketOfferList = marketOfferList;
    }
    
    public MarketOffer addMarketOffer(){
        MarketOffer marketOffer = new MarketOffer();
        marketOfferList.add(marketOffer);
        return marketOffer;
    }
    public void removeMarketOffer(MarketOffer marketoffer){
        marketOfferList.remove(marketoffer);
    }
    
    
    
    public MarketOffer getMarketOffer(int marketID,int productID)
    {
        
        for (MarketOffer mO:marketOfferList)
        {
            if(mO.getMarketID()==marketID&&mO.getProductID()==productID)
            {
                return mO;
            }
        }
        return null;
    }
    
}
