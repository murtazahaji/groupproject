/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author murta
 */
public class Order {
    
    private String orderStatus;
    private String issueDate;
    private String compensationDate;
    private String shippingDate;
    private SalesPerson salesPerson;    
    private ArrayList<OrderItem> orderItemList;

    public Order() {
        orderItemList = new ArrayList<OrderItem>();
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getCompensationDate() {
        return compensationDate;
    }

    public void setCompensationDate(String compensationDate) {
        this.compensationDate = compensationDate;
    }

    public String getShippingDate() {
        return shippingDate;
    }

    public void setShippingDate(String shippingDate) {
        this.shippingDate = shippingDate;
    }

    public SalesPerson getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(SalesPerson salesPerson) {
        this.salesPerson = salesPerson;
    }

    public ArrayList<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(ArrayList<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }
    
    public OrderItem addOrderItem(Product p,int q,double price){
        OrderItem orderItem = new OrderItem();
        orderItem.setProduct(p);
        orderItem.setQuantity(q);
        orderItem.setPaidPrice(price);
        orderItemList.add(orderItem);
        return orderItem;
    }
    
    
    
}
