/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author murta
 */
public class SalesPerson {
    
    private String firstName;
    private String lastName;
    private int commission;
    private float sales;
    private float target;
    private int salesPersonId;
    private UserAccount userAccount;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getCommission() {
        return commission;
    }

    public void setCommission(int commission) {
        this.commission = commission;
    }

    public void setSales(float sales) {
        this.sales = sales;
    }

    public void setTarget(float target) {
        this.target = target;
    }

    public void setSalesPersonId(int salesPersonId) {
        this.salesPersonId = salesPersonId;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    public int getSalesPersonId() {
        return salesPersonId;
    }

    
    
    
}

