/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author murta
 */
public class ProductDirectory {
    
    private ArrayList<Product> productList;

    public ProductDirectory() {
        productList = new ArrayList<Product>();
    }

    public ArrayList<Product> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<Product> productList) {
        this.productList = productList;
    }

    public Product addProduct() {
 
        Product product = new Product();
        productList.add(product);
        return product;
    }

    public void removeProduct(Product p) {    
        productList.remove(p);
    }

    public Product searchProduct(int productId) {
       
        for(Product product : productList){
            if(product.getProductID()== productId){
                return product;
            }                
        }
        return null;
    }
    
    public int getLatestProductID()
    {
        if(!productList.isEmpty())
        {
            return productList.get(productList.size()-1).getProductID();
        }
        return 0;
    }
}