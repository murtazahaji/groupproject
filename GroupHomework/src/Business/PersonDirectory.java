/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author dhair
 */
public class PersonDirectory {
    
    private ArrayList<Person> personList;
    
    public PersonDirectory(){
        personList = new ArrayList<Person>();
    }

    public ArrayList<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(ArrayList<Person> personList) {
        this.personList = personList;
    }
    
    
    public Person addPerson()
    {
        Person p = new Person();
        personList.add(p);
        return p;
    
    }
    
    public void removePerson(Person person){
        personList.remove(person);
    }
    
    public Person getPersonByID(Integer personID){
        for(Person person : personList){
            if(person.getPersonID().equals(personID)){
                return person;
            }
        }
        return null;
    }
    
    public Integer getLatestPersonID(){
        Person person = personList.get(personList.size()-1);
        System.out.println(personList.size());
        int pID = (int)person.getPersonID();
        return pID;
    }
}

