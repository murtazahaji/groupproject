/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author murta
 */
public class Market {
    
    private int marketID;
    private String marketDescription;
    private String marketType;
    private String marketOffer;
    private static int count = 0;
    
    public Market(){
        count++;
        marketID = count;
    }
    
    public String getMarketType() {
        return marketType;
    }

    public void setMarketType(String marketType) {
        this.marketType = marketType;
    }

    public String getMarketOffer() {
        return marketOffer;
    }

    public void setMarketOffer(String marketOffer) {
        this.marketOffer = marketOffer;
    }

    public int getMarketID() {
        return marketID;
    }

    public void setMarketID(int marketID) {
        this.marketID = marketID;
    }

    public String getMarketDescription() {
        return marketDescription;
    }

    public void setMarketDescription(String marketDescription) {
        this.marketDescription = marketDescription;
    }
    
    
    
}
