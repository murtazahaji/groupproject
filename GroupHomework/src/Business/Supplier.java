/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author murta
 */
public class Supplier {
    
    private String supplierName;
    private int supplierID;
    private ProductDirectory productDirectory;
    private static int count = 0;
    
    public Supplier() {
    
        count++;
        supplierID = count;
        productDirectory = new ProductDirectory();
        
    }

    public ProductDirectory getProductDirectory() {
        return productDirectory;
    }

    public void setProductDirectory(ProductDirectory productDirectory) {
        this.productDirectory = productDirectory;
    }
    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public int getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(int supplierID) {
        this.supplierID = supplierID;
    }
     public void addToSupplier(Product product){
        productDirectory.getProductList().add(product);
    }
    
    @Override
        public String toString(){
            return this.getSupplierName();
        }
        

}
