/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.BusinessConfig;

import Business.Business;
import Business.Market;
import Business.MarketDirectory;
import Business.Person;
import Business.PersonDirectory;
import Business.Product;
import Business.ProductDirectory;
import Business.Supplier;
import Business.SupplierDirectory;
import Business.UserAccount;
import Business.UserAccountDirectory;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author dhair
 */
public class ConfigureABusiness {

    public static Business Initialize(String n) {
        Business business = new Business();
        business.setName(n);
           
        LoadSupplier(business);
        LoadProducts(business);
        LoadPersonDirectory(business);
        LoadUserAccountDirectory(business);
        LoadMarketList(business);
        return business;
        
//        Person p1 = personDirectory.addPerson();
//        p1.setFirstName("Murtaza");
//        p1.setLastName("Haji");   
        
//        Person p2 = personDirectory.addPerson();
//        p2.setFirstName("Murtaza");
//        p2.setLastName("Haji");
//       
//        Person p3 = personDirectory.addPerson();
//        p3.setFirstName("Murtaza");
//        p3.setLastName("Haji");
//        
//        Person p4 = personDirectory.addPerson();
//        p4.setFirstName("Murtaza");
//        p4.setLastName("Haji");
//        
//        UserAccountDirectory userAccountDirectory = business.getUserAccountDirectory();
//        
//        UserAccount ua1 = userAccountDirectory.addUserAccount();
//        ua1.setUserId("systemadmin");
//        ua1.setPassword("1");
//        ua1.setAccountRole("System Admin");
//        
//        UserAccount ua2 = userAccountDirectory.addUserAccount();
//        ua2.setUserId("customer");
//        ua2.setPassword("1");
//        ua2.setAccountRole("Customer");
//        
//        UserAccount ua3 = userAccountDirectory.addUserAccount();
//        ua3.setUserId("supplier");
//        ua3.setPassword("1");
//        ua3.setAccountRole("Supplier");
//        
//        UserAccount ua4 = userAccountDirectory.addUserAccount();
//        ua4.setUserId("manager");
//        ua4.setPassword("1");
//        ua4.setAccountRole("HR");
//        
//        ua1.setPerson(p1);
//        ua2.setPerson(p1);
//        ua3.setPerson(p1);
//        ua4.setPerson(p1);
//        p1.addToPerson(ua1);
//        p1.addToPerson(ua2);
//        p1.addToPerson(ua3);
//        p1.addToPerson(ua4);
//        
//        SupplierDirectory supplierDirectory = business.getSupplierDirectory();
//        Supplier s1 = supplierDirectory.addSupplier();
//        
//        s1.setSupplierName("Apple");
//        s1.setSupplierID(1);
//        
//        Supplier s2 = supplierDirectory.addSupplier();
//        s2.setSupplierName("Google");
//        s2.setSupplierID(2);
//        
//        Supplier supplier = new Supplier();
//        
//        ProductDirectory productDirectory = supplier.getProductDirectory();
//        Product product1 = productDirectory.addProduct();
////        product1.setProductID(1);
//        product1.setAvailability(true);
//        product1.setProductName("IPhone");
//        product1.setDescription("Expensive");
//        product1.setPrice(999);
//        
//        Product product2 = productDirectory.addProduct();
//        product2.setProductName("IPods");
//        product2.setDescription("Chutzpah");
//        product2.setAvailability(true);
////        product2.setProductID(2);
//        product2.setPrice(299);
//        
//        Product product3 = productDirectory.addProduct();
//        product2.setProductName("Pixel2");
//        product2.setDescription("Slightly Expensive");
//        product2.setAvailability(true);
////        product2.setProductID(3);
//        product2.setPrice(649); 
//        
//        Product product4 = productDirectory.addProduct();
//        product2.setProductName("Chromebook");
//        product2.setDescription("Average");
//        product2.setAvailability(true);
////        product2.setProductID(4);
//        product2.setPrice(799);
//        
//        product1.setSupplier(s1);
//        product2.setSupplier(s1);
//        product3.setSupplier(s2);
//        product4.setSupplier(s2);
//        s1.addToSupplier(product1);
//        s1.addToSupplier(product2);
//        s2.addToSupplier(product3);
//        s2.addToSupplier(product4);
//        
//        return business;
//
    }

    public static void LoadPersonDirectory(Business business) {

        String csvfile = System.getProperty("user.dir")+"\\src\\CSV\\person.csv";
        BufferedReader BR = null;
        String Line = "";
        String csvsplitby = ",";
        PersonDirectory personDirectory = business.getPersonDirectory();
        try {
            BR = new BufferedReader(new FileReader(csvfile));
           
            while ((Line = BR.readLine()) != null) {
                //System.out.println(Line);
                String[] Content = Line.split(csvsplitby);
                Person person = personDirectory.addPerson();
                person.setFirstName(Content[0]);
                person.setLastName(Content[1]);
                person.setSocialSecurityNumber(Double.parseDouble(Content[3]));
                person.setDateOfBirth(Content[2]);
                person.setAddress(Content[4]);
                person.setPersonID(Integer.parseInt(Content[5]));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void LoadSupplier(Business business) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        String csvfile = System.getProperty("user.dir")+"\\src\\CSV\\supplier.csv";
        BufferedReader BR = null;
        String Line = "";
        String csvsplitby = ",";
        SupplierDirectory supplierDirectory = business.getSupplierDirectory();
        try {
            BR = new BufferedReader(new FileReader(csvfile));
           
            while ((Line = BR.readLine()) != null) {
                //System.out.println(Line);
                String[] Content = Line.split(csvsplitby);
                Supplier supplier = supplierDirectory.addSupplier();
                supplier.setSupplierName(Content[0]);
                supplier.setSupplierID(Integer.parseInt(Content[1]));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void LoadProducts(Business business) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        String csvfile = System.getProperty("user.dir")+"\\src\\CSV\\product.csv";
        BufferedReader BR = null;
        String Line = "";
        String csvsplitby = ",";
        ProductDirectory productDirectory = business.getProductDirectory();
        try {
            BR = new BufferedReader(new FileReader(csvfile));
           
            while ((Line = BR.readLine()) != null) {
                //System.out.println(Line);
                String[] Content = Line.split(csvsplitby);
                Product product = productDirectory.addProduct();
                product.setProductName(Content[0]);
                product.setProductID(Integer.parseInt(Content[1]));
                product.setAvailability(Integer.parseInt(Content[2]));
                product.setDescription(Content[3]);
                product.setProductPrice(Integer.parseInt(Content[4]));
//                person.setPersonID(Integer.parseInt(Content[5]));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static void LoadUserAccountDirectory(Business business) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        String csvfile = System.getProperty("user.dir")+"\\src\\CSV\\useraccount.csv";
        BufferedReader BR = null;
        String Line = "";
        String csvsplitby = ",";
        UserAccountDirectory userAccountDirectory = business.getUserAccountDirectory();
        try {
            BR = new BufferedReader(new FileReader(csvfile));
           
            while ((Line = BR.readLine()) != null) {
                //System.out.println(Line);
                String[] Content = Line.split(csvsplitby);
                UserAccount userAccount = userAccountDirectory.addUserAccount();
                userAccount.setUserId(Content[0]);
                userAccount.setPassword(Content[1]);
                userAccount.setAccountRole(Content[2]);
                userAccount.setStatus(Boolean.parseBoolean(Content[3]));
                userAccount.setCreatedOn(Content[4]);
                userAccount.setOrganisation(Content[5]);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void LoadMarketList(Business business) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        String csvfile = System.getProperty("user.dir")+"\\src\\CSV\\market.csv";
        BufferedReader BR = null;
        String Line = "";
        String csvsplitby = ",";
        MarketDirectory marketDirectory = business.getMarketDirectory();
        try {
            BR = new BufferedReader(new FileReader(csvfile));
           
            while ((Line = BR.readLine()) != null) {
                //System.out.println(Line);
                String[] Content = Line.split(csvsplitby);
                Market market = marketDirectory.addMarket();
                market.setMarketType(Content[0]);
                market.setMarketDescription(Content[1]);                
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

