/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author murta
 */
public class Customer {
    
    private String customerFirstName;
    private String customerLastName;
    private int customerID;
    private String address;
    private int market_ID;
    private ArrayList<Order> orderList;
    
   private static int count =0;
   public Customer(){
    count++;
    customerID = count;
    orderList = new ArrayList<Order>();
    
    }

    public int getMarket_ID() {
        return market_ID;
    }

    public void setMarket_ID(int market_ID) {
        this.market_ID = market_ID;
    }
   
   

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ArrayList<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(ArrayList<Order> orderList) {
        this.orderList = orderList;
    }

    public Order addOrder(Order order){
        orderList.add(order);
        return order;
    
    }
    
     public void removeOrderItem(OrderItem order) {    
        orderList.remove(order);
    }
    
    
    
    
}
