package Interface.SupplierRole;

import Business.Business;
import Business.Product;
import Business.ProductDirectory;
import Business.Supplier;
import Business.UserAccount;
import Interface.AdministrativeRole.ViewSupplier;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


/**
 *
 * @author Mihir Mehta / Hechen Gao
 */
public class CreateNewProductJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
//    Supplier supplier;
    private Business business;
    private Supplier supplier;
    private UserAccount userAccount;
    public CreateNewProductJPanel(JPanel userProcessContainer, Business business, UserAccount userAccount, Supplier supplier){
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.business = business;
        this.supplier = supplier;
        this.userAccount = userAccount;
    }
    
    private void backAction() {
        userProcessContainer.remove(this);
        Component[] componentArray = userProcessContainer.getComponents();
        Component component = componentArray[componentArray.length - 1];
        ManageProductCatalogJPanel manageProductCatalogJPanel = (ManageProductCatalogJPanel) component;
        manageProductCatalogJPanel.refreshTable();
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtPrice = new javax.swing.JTextField();
        btnAdd = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtDescription = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        radioAV2 = new javax.swing.JRadioButton();
        radioAv1 = new javax.swing.JRadioButton();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Create New Product");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 20, -1, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Model No.");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 130, 100, 30));

        txtId.setEditable(false);
        txtId.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtId.setEnabled(false);
        add(txtId, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 130, 210, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Availability");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 280, 110, 30));

        txtPrice.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        add(txtPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 230, 160, 30));

        btnAdd.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnAdd.setText("Save");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        add(btnAdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 350, -1, -1));

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 480, -1, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("Product Name:");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 90, -1, 30));

        txtName.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        add(txtName, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 90, 210, -1));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setText("Description:");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 180, 110, 30));

        txtDescription.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        add(txtDescription, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 180, 160, 30));

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setText("Product Price:");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 230, 110, 30));

        radioAV2.setText("Not Available");
        add(radioAV2, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 310, -1, -1));

        radioAv1.setText("Available");
        add(radioAv1, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 270, -1, -1));
    }// </editor-fold>//GEN-END:initComponents
    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed

        if(txtDescription.getText().equals("") || txtId.getText().equals("") || txtName.getText().equals("") || txtPrice.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Please fill in all the details","Warning",JOptionPane.WARNING_MESSAGE);
            return;
        }
        else
        {
            ProductDirectory productDirectory = business.getSupplierDirectory().getSupplierByID(supplier.getSupplierID()).getProductDirectory();
            Product product = productDirectory.addProduct();
        
        product.setProductName(txtName.getText());
        product.setDescription(txtDescription.getText());
        product.setModelNumber(txtId.getText());
        product.setProductPrice(Integer.parseInt(txtPrice.getText()));
        if(radioAv1.isSelected())
        product.setAvailability(true);
        else if(radioAV2.isSelected())
        product.setAvailability(false);
        
          JOptionPane.showMessageDialog(null, "Product Added Successfully.","Warning",JOptionPane.WARNING_MESSAGE);
        }


}//GEN-LAST:event_btnAddActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        
        backAction();
        
    }//GEN-LAST:event_btnBackActionPerformed
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnBack;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JRadioButton radioAV2;
    private javax.swing.JRadioButton radioAv1;
    private javax.swing.JTextField txtDescription;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtPrice;
    // End of variables declaration//GEN-END:variables
}
