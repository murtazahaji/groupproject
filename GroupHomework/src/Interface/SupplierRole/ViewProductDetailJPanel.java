package Interface.SupplierRole;

import Business.Business;
import Business.Product;
import Business.Supplier;
import Business.UserAccount;
import Interface.AdministrativeRole.ManageSuppliers;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Mihir Mehta / Hechen Gao
 */
public class ViewProductDetailJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private Product product;
    private Business business;
    private Supplier supplier;
    private UserAccount userAccount;
    public ViewProductDetailJPanel(JPanel userProcessContainer, Business business,UserAccount userAccount, Supplier supplier, Product product) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.product = product;
        this.business = business;
        this.userAccount = userAccount;
        this.supplier = supplier;
        txtName.setText(product.getProductName());
        txtId.setText(String.valueOf(product.getProductID()));
        txtModelNumber.setText(String.valueOf(product.getProductPrice()));
        populateDetails();
    }
    
    public void populateDetails(){
        txtName.setText(product.getProductName());
        txtId.setText(String.valueOf(product.getProductID()));
        if(product.getAvailability()){
            radio1.setSelected(true);
        }else{
            radio2.setSelected(true);
        }
       txtDesc.setText(product.getDescription());
       txtModelNumber.setText(product.getModelNumber());
       txtModelNumber.setText(String.valueOf(product.getProductPrice()));
       txtId.setEnabled(false);
        txtName.setEnabled(false);
        txtDesc.setEnabled(false);
        txtModelNumber.setEnabled(false);
        btnSave.setEnabled(false);
        radio1.setEnabled(false);
        radio2.setEnabled(false);
        btnUpdate.setEnabled(true);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtModelNumber = new javax.swing.JTextField();
        btnUpdate = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        txtId = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        txtDesc = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        radio1 = new javax.swing.JRadioButton();
        radio2 = new javax.swing.JRadioButton();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("View Product Detail");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(135, 25, -1, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Product Name:");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, -1, 30));

        txtName.setEditable(false);
        txtName.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtName.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        add(txtName, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 90, 159, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Description");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 170, -1, 30));

        txtModelNumber.setEditable(false);
        txtModelNumber.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtModelNumber.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        add(txtModelNumber, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 220, 159, -1));

        btnUpdate.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnUpdate.setText("Update Product");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        add(btnUpdate, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 350, 176, -1));

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 500, -1, -1));

        txtId.setEditable(false);
        txtId.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtId.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        add(txtId, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 130, 159, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("Product ID:");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 130, -1, 30));

        btnSave.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSave.setText("SAVE");
        btnSave.setEnabled(false);
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        add(btnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 350, 70, 30));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setText("Availability");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 270, 130, 30));

        txtDesc.setEditable(false);
        txtDesc.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtDesc.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        add(txtDesc, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 170, 159, -1));

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setText("Model Number");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 220, 130, 30));

        radio1.setText("Available");
        add(radio1, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 260, -1, -1));

        radio2.setText("Not Available");
        add(radio2, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 300, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed

        //txtId.setEnabled(true);
        txtName.setEnabled(true);
        txtDesc.setEnabled(true);
        txtModelNumber.setEnabled(true);
        btnSave.setEnabled(true);
        radio1.setEnabled(true);
        radio1.setEnabled(true);
        btnUpdate.setEnabled(false);
}//GEN-LAST:event_btnUpdateActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed

        backAction();
    }//GEN-LAST:event_btnBackActionPerformed

      private void backAction() {
        userProcessContainer.remove(this);
        Component[] componentArray = userProcessContainer.getComponents();
        Component component = componentArray[componentArray.length - 1];
        ManageProductCatalogJPanel manageProductCatalogJPanel = (ManageProductCatalogJPanel) component;
        manageProductCatalogJPanel.refreshTable();
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }
    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        
        if(txtDesc.equals("") || txtModelNumber.equals("") || txtName.equals("")){
            JOptionPane.showMessageDialog(null, "Please fill in all the details","Warning",JOptionPane.WARNING_MESSAGE);
            return;
        }
        else{
        Product p1= business.getSupplierDirectory().getSupplierByID(supplier.getSupplierID()).getProductCatalog().searchProduct(product.getProductID());
        
        
        p1.setProductName(txtName.getText());
        p1.setDescription(txtDesc.getText());
        p1.setModelNumber(txtModelNumber.getText());
        if(radio1.isSelected())
            p1.setAvailability(true);
        else if(radio2.isSelected())
            p1.setAvailability(false);
//        p1.setPrice(Integer.parseInt(txtPrice.getText()));
       
         txtId.setEnabled(false);
        txtName.setEnabled(false);
        txtDesc.setEnabled(false);
        txtModelNumber.setEnabled(false);
        btnSave.setEnabled(false);
        radio1.setEnabled(false);
        radio2.setEnabled(false);
//        txtPrice.setEnabled(true);
        btnUpdate.setEnabled(true);
     
          JOptionPane.showMessageDialog(null, "Product updated successfully.","Warning",JOptionPane.WARNING_MESSAGE);
         
        }
        
    }//GEN-LAST:event_btnSaveActionPerformed
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JRadioButton radio1;
    private javax.swing.JRadioButton radio2;
    private javax.swing.JTextField txtDesc;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtModelNumber;
    private javax.swing.JTextField txtName;
    // End of variables declaration//GEN-END:variables
}
