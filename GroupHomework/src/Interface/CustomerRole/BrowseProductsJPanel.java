/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.CustomerRole;

import Business.Business;
import Business.Customer;
import Business.Order;
import Business.OrderItem;
import Business.Product;
import Business.Supplier;
import com.sun.javafx.tk.quantum.MasterTimer;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author murta
 */
public class BrowseProductsJPanel extends javax.swing.JPanel {

    /**
     * Creates new form BrowseProductsJPanel
     */
    private JPanel userProcessContainer;
    private Business business;
    private Product product;
    private Order order;
    private Customer customer;
    private boolean isCheckedOut=false;
   
    
    public BrowseProductsJPanel(JPanel  userProcessContainer,Business business,Product product) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.business = business;
        this.product = product;
        populateSupplierComboBox();
        if(!isCheckedOut)
        order = new Order();
        
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    
    public void populateSupplierComboBox(){
        suppComboBox1.removeAllItems();
        for(Supplier s : business.getSupplierDirectory().getSupplierList()){
            suppComboBox1.addItem(s);
        }
        populateTable();
    }
    
    public void populateTable(){
        DefaultTableModel dtm = (DefaultTableModel) productDirectoryJTable.getModel();
        dtm.setRowCount(0);
        Supplier supplier = (Supplier)suppComboBox1.getSelectedItem();
        for(Product p : supplier.getProductDirectory().getProductList()){
            Object row[] = new Object[4];
            row[0] = p;
            row[1] = p.getProductID();
            row[2] = p.getProductPrice();
            row[3] = p.getAvailability();
            dtm.addRow(row);
        }
    }
    
    public void refreshProductTable(String keyword){
        
        DefaultTableModel dtm = (DefaultTableModel) productDirectoryJTable.getModel();
        dtm.setRowCount(0);
        for(Supplier supplier : business.getSupplierDirectory().getSupplierList()){
            for(Product p : supplier.getProductDirectory().getProductList()){
                if(p.getProductName().equalsIgnoreCase(keyword)){
                    Object row[] = new Object[4];
                    row[0] = p;
                    row[1] = p.getProductID();
                    row[2] = p.getProductPrice();
                    row[3] = p.getAvailability();
                    dtm.addRow(row);
                }
            }
        }
    }
    public void refreshOrderTable(){
        
        DefaultTableModel dtm = (DefaultTableModel) orderTable.getModel();
        dtm.setRowCount(0);
        for(OrderItem orderItem :order.getOrderItemList()){
            
                    Object row[] = new Object[4];
                    row[0] = orderItem.getProduct();
                    row[1] = orderItem.getPaidPrice();
                    row[2] = orderItem.getQuantity();
                    row[3] = orderItem.getQuantity()* orderItem.getPaidPrice();
                    dtm.addRow(row);
                }
            }
        


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        suppComboBox1 = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        txtSearchKeyWord = new javax.swing.JTextField();
        btnSearchProduct = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        productDirectoryJTable = new javax.swing.JTable();
        viewProdjButton2 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        txtSalesPrice = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        quantitySpinner = new javax.swing.JSpinner();
        addtoCartButton6 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        orderTable = new javax.swing.JTable();
        btnViewOrderItem = new javax.swing.JButton();
        txtNewQuantity = new javax.swing.JTextField();
        btnModifyQuantity = new javax.swing.JButton();
        btnCheckOut = new javax.swing.JButton();
        btnRemoveOrderItem = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();

        jLabel4.setBackground(new java.awt.Color(0, 0, 0));
        jLabel4.setFont(new java.awt.Font("Vijaya", 1, 36)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(51, 51, 255));
        jLabel4.setText("BROWSE PRODUCTS");
        jLabel4.setIconTextGap(7);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Supplier");

        suppComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                suppComboBox1ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Supplier Product Catalog");

        btnSearchProduct.setText("Search Product");
        btnSearchProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchProductActionPerformed(evt);
            }
        });

        productDirectoryJTable.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        productDirectoryJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Name", "Product Id", "Price", "Avail"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(productDirectoryJTable);

        viewProdjButton2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        viewProdjButton2.setText("View Product Detail");
        viewProdjButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewProdjButton2ActionPerformed(evt);
            }
        });

        jLabel6.setText("Sales Price");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Quantity:");

        quantitySpinner.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));

        addtoCartButton6.setText("ADD TO CART");
        addtoCartButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addtoCartButton6ActionPerformed(evt);
            }
        });

        orderTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Item Name", "Price", "Quantity", "Total Amount"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(orderTable);

        btnViewOrderItem.setText("View Item");
        btnViewOrderItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewOrderItemActionPerformed(evt);
            }
        });

        btnModifyQuantity.setText("Modify Quantity");
        btnModifyQuantity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModifyQuantityActionPerformed(evt);
            }
        });

        btnCheckOut.setText("Check out");
        btnCheckOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckOutActionPerformed(evt);
            }
        });

        btnRemoveOrderItem.setText("Remove");
        btnRemoveOrderItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveOrderItemActionPerformed(evt);
            }
        });

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(130, 130, 130)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 419, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnViewOrderItem)
                        .addGap(26, 26, 26)
                        .addComponent(txtNewQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnModifyQuantity)
                        .addGap(26, 26, 26)
                        .addComponent(btnCheckOut)
                        .addGap(39, 39, 39)
                        .addComponent(btnRemoveOrderItem))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 580, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(suppComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtSearchKeyWord, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnSearchProduct))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(viewProdjButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(42, 42, 42)
                            .addComponent(jLabel6)
                            .addGap(18, 18, 18)
                            .addComponent(txtSalesPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(39, 39, 39)
                            .addComponent(jLabel5)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(quantitySpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(addtoCartButton6))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 590, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(180, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addGap(32, 32, 32)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(suppComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSearchKeyWord, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSearchProduct))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(viewProdjButton2)
                    .addComponent(jLabel6)
                    .addComponent(txtSalesPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(quantitySpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addtoCartButton6))
                .addGap(54, 54, 54)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnViewOrderItem)
                    .addComponent(txtNewQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnModifyQuantity)
                    .addComponent(btnCheckOut)
                    .addComponent(btnRemoveOrderItem))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnBack)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void suppComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_suppComboBox1ActionPerformed
//         TODO add your handling code here:
          populateTable();
    }//GEN-LAST:event_suppComboBox1ActionPerformed

    private void btnSearchProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchProductActionPerformed
        String productName = txtSearchKeyWord.getText();
        refreshProductTable(productName);
    }//GEN-LAST:event_btnSearchProductActionPerformed

    private void viewProdjButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewProdjButton2ActionPerformed
        // TODO add your handling code here:
        int row = productDirectoryJTable.getSelectedRow();
        if(row<0){
            JOptionPane.showMessageDialog(null, "Pls select a row!!", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        Product p = (Product)productDirectoryJTable.getValueAt(row, 0);
        ViewProductDetailJPanel vpdjp = new ViewProductDetailJPanel(userProcessContainer,business,product);
        userProcessContainer.add("ViewProductDetailJPanelCustomer", vpdjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);

    }//GEN-LAST:event_viewProdjButton2ActionPerformed

    private void addtoCartButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addtoCartButton6ActionPerformed
        // TODO add your handling code here:
        int selectedRow = productDirectoryJTable.getSelectedRow();
        if(selectedRow<0)
        {
           JOptionPane.showMessageDialog(null,"Please select a row" );
           return;
        }
        Product selectedProduct =(Product)productDirectoryJTable.getValueAt(selectedRow,0);
        int fetchQty = (Integer)quantitySpinner.getValue();
        if(fetchQty <=0){
            JOptionPane.showMessageDialog(null,"Quantity Cannot be Zero" );
           return;
        }
        try
        {
        double salesPrice =Double.parseDouble(txtSalesPrice.getText());
        if(salesPrice <= selectedProduct.getProductPrice()){
        JOptionPane.showMessageDialog(null,"SalesPrice cannot be less that actual price" );
        return;
        }
        if(fetchQty <= selectedProduct.getAvailability()){
        boolean alreadyPresent = false;
        for(OrderItem orderItem : order.getOrderItemList() ){
        if (orderItem.getProduct()== selectedProduct){
               int oldAvail = selectedProduct.getAvailability();
               int newAvail = oldAvail - fetchQty;
               selectedProduct.setAvailability(newAvail);
               orderItem.setQuantity(fetchQty+orderItem.getQuantity());
               alreadyPresent=true;
               populateTable();
               refreshOrderTable();
               break;
        }
        }
        if(!alreadyPresent){
        int oldAvail = selectedProduct.getAvailability();
        int newAvail = oldAvail - fetchQty;
        selectedProduct.setAvailability(newAvail);
        order.addOrderItem(selectedProduct,fetchQty,salesPrice);
        populateTable();
        refreshOrderTable();
        
        }
        }
        else{
        JOptionPane.showMessageDialog(null,"Quantity>Availabilty","Warning",JOptionPane.WARNING_MESSAGE);
            
        }
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,"Invalid SalesPrice" );
            return;
        }
        

    }//GEN-LAST:event_addtoCartButton6ActionPerformed

    private void btnViewOrderItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewOrderItemActionPerformed
int row = orderTable.getSelectedRow();
        if(row<0){
            JOptionPane.showMessageDialog(null, "Pls select a row!!", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        OrderItem orderItem = (OrderItem)orderTable.getValueAt(row, 0);
        ViewOrderItemDetailJPanel vpdjp = new ViewOrderItemDetailJPanel(userProcessContainer,business,orderItem);
        userProcessContainer.add("ViewProductDetailJPanelCustomer", vpdjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnViewOrderItemActionPerformed

    private void btnModifyQuantityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModifyQuantityActionPerformed
        // TODO add your handling code here:
        int selectedRow = orderTable.getSelectedRow();
        if(selectedRow<0)
        {
        return;
        }
        OrderItem orderItem = (OrderItem)orderTable.getValueAt(selectedRow,0);
        int currentAvail = orderItem.getProduct().getAvailability();
        int oldQty = orderItem.getQuantity();
        int newQty = Integer.parseInt(txtNewQuantity.getText());
        if(newQty>(currentAvail+oldQty))
        {
        JOptionPane.showMessageDialog(null,"Quantity is more than availability" );
            return;
        }
        else
        {
        if (newQty <=0){
        JOptionPane.showMessageDialog(null,"Quantity cannot be 0" );
            return;
        }
        orderItem.setQuantity(newQty);
        orderItem.getProduct().setAvailability(currentAvail+(oldQty-newQty));
        refreshOrderTable();
        populateTable();
        }

    }//GEN-LAST:event_btnModifyQuantityActionPerformed

    private void btnCheckOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckOutActionPerformed
        // TODO add your handling code here:
        if(order.getOrderItemList().size()>0){
         customer.addOrder(order);
         JOptionPane.showMessageDialog(null,"Order Placed Successfully");
         order = new Order();
         refreshOrderTable();
         populateTable();
         isCheckedOut = true;
        }
        else{
        JOptionPane.showMessageDialog(null,"No Order Placed");
        }

    }//GEN-LAST:event_btnCheckOutActionPerformed

    private void btnRemoveOrderItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveOrderItemActionPerformed
  int row = orderTable.getSelectedRow();
        if(row<0){
            JOptionPane.showMessageDialog(null, "Pls select a row!!", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        OrderItem orderItem = (OrderItem)orderTable.getValueAt(row, 0);
        int oldAvail = orderItem.getProduct().getAvailability();
        int oldQty = orderItem.getQuantity();
        int newQty = oldAvail + oldAvail;
        orderItem.getProduct().setAvailability(newQty);
        customer.removeOrderItem(orderItem);
        JOptionPane.showMessageDialog(null,"Item Removed from cart");
        refreshOrderTable();
        populateTable();
    }//GEN-LAST:event_btnRemoveOrderItemActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);


    }//GEN-LAST:event_btnBackActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addtoCartButton6;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCheckOut;
    private javax.swing.JButton btnModifyQuantity;
    private javax.swing.JButton btnRemoveOrderItem;
    private javax.swing.JButton btnSearchProduct;
    private javax.swing.JButton btnViewOrderItem;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable orderTable;
    private javax.swing.JTable productDirectoryJTable;
    private javax.swing.JSpinner quantitySpinner;
    private javax.swing.JComboBox suppComboBox1;
    private javax.swing.JTextField txtNewQuantity;
    private javax.swing.JTextField txtSalesPrice;
    private javax.swing.JTextField txtSearchKeyWord;
    private javax.swing.JButton viewProdjButton2;
    // End of variables declaration//GEN-END:variables
}
